Exercise 1: Managing control flow
=================================

With this exercise we will learn to describe the dependencies in control flow 
between Fireworks, exploit possible concurrencies and use the standard Firetask 
``ScriptTask``. 

Problem 1.1
-----------

Change to directory **exercises/work/1_control_flow**.

Copy the workflow files from the folder **exercises/problems/1_control_flow**. 
For example, if you decide to use YAML::
    
    cp ../../problems/1_control_flow/*.yaml .

One by one, check the three sequential workflows **f1_pitstop_seq_wrong_1.yaml**, 
**f1_pitstop_seq_wrong_2.yaml**, and **f1_pitstop_seq_wrong_3.yaml** for errors, 
and then find and correct the errors.

Add the workflow to the LaunchPad and query its state.

Execute the workflow with ``rlaunch singleshot``, i.e. run one Firework at a time.

After running each single Firework, monitor the output and the states of the 
Fireworks until the workflow is completed.


Problem 1.2
-----------

Repeat the steps of **Problem 1.1** for the parallel version of the workflow:
**f1_pitstop_par_wrong_1.json**, **f1_pitstop_par_wrong_2.json** and 
**f1_pitstop_par_wrong_3.json**.
