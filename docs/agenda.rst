Basic procedures
================

 * Compose Fireworks and workflows
 * Validate workflows
 * View workflows
 * Add fireworks to LaunchPad: ``lpad add``
 * Execution: ``rlaunch``
 * Monitoring: ``lpad get_*``, ``lpad webgui``

Exercise 1: Managing control flow 
=================================

 * Dependencies and concurrency
 * Use standard Firetasks: ``ScriptTask``
 * Example: F1 pitstop

Exercise 2: Managing data flow
==============================

 * Data flow dependencies 
 * Use custom Firetasks: ``PythonFunctionTask``
 * Example: Recruiting

Exercise 3: Manage data in files and command line input
=======================================================

 * Use custom Firetasks: ``CommandLineTask``
 * Example: Image reconstruction

Exercise 4: Extending a workflow
================================

 * Extension of example from Exercise 3
 * Example: Image swirl

Exercise 5: Writing a Firetask
==============================
 * Extension of example from Exercise 2

