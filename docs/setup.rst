Setup
=====

Prerequisites
-------------

Python
~~~~~~

For this tutorial it is recommended to use the anaconda python distribution. 
Download anaconda (Python 3.6 version) from https://www.continuum.io/downloads 
for Linux x86_64 and follow the installation instructions on the download page. 
Make sure that python from this installation is in your ``$PATH``. In addition 
the following python packages must be installed::

    pip install --upgrade pip
    pip install pjson
    pip install pyaml
    pip install future
    pip install python-igraph

**Hint:** If later, during usage of the igraph library, an error like this::

    ImportError: /home/gks/anaconda3/lib/python3.6/site-packages/igraph/_igraph.cpython-36m-x86_64-linux-gnu.so: undefined symbol: _ZNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEC1Ev

occurs, then the libgcc package has to be installed/upgraded::

    conda install libgcc


MongoDB
~~~~~~~

If your system is Ubuntu and you have administrator permissions you can install
MongoDB system-wide with the following command::

    sudo apt-get install mongodb

The server is automatically started after the installation is completed.

For general instructions please read the installation manual
https://docs.mongodb.com/manual/administration/install-on-linux/

Install FireWorks
-----------------

The following instructions are for installation in your ``$HOME`` directory. 
You might like to create another directory for this tutorial and replace 
``$HOME`` with the relevant path.

::

    cd $HOME
    git clone https://github.com/ikondov/fireworks
    cd fireworks
    python setup.py develop --user
    export PYTHONPATH=$PWD:$PYTHONPATH
    export PATH=$HOME/.local/bin:$PATH

Install the tutorial
--------------------
::

    cd $HOME
    git clone https://git.scc.kit.edu/jk7683/gridka-school-fireworks
    cd gridka-school-fireworks
    export PYTHONPATH=$PWD/lib:$PYTHONPATH
    export PATH=$PWD/bin:$PATH

Further packages
----------------

In order to visualize the workflows graphically two packages must be installed::

    sudo apt-get install graphviz evince

The packages ImageMagick and Eye of GNOME (eog) are necessary for Exercise 3::

    sudo apt-get install imagemagick eog

If editors like ``vi`` or ``nano`` are not preferred, more advanced editors may
be installed, e.g.::

    sudo apt-get install gedit emacs

For the web GUI of lpad a web browser must be installed, e.g.::

    sudo apt-get install firefox
