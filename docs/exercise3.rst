Exercise 3: Manage data in files and command line input
=======================================================

The purpose of this exercise is to learn how to pass data between Fireworks as 
files and process these data using the custom ``CommandLineTask``. The built-in 
``ScriptTask`` used in **Exercise 1** allows to run a script but provides no
methods to move data between Fireworks and no handling of command line options, flags 
input and output as workflow data. 

Given is a set of reusable operations implemented using the ``convert`` and 
``montage`` commands from the *ImageMagick* package with different sets of 
command line flags. These are:

    - Rotate +/-90°, 180° 
    - Horizontal mirror
    - Vertical mirror
    - Montage
    - Swirl
    - Animate

The Fireworks corresponding to these operations can be found in 
**exercises/demos/3_files_and_commands**. The provided solutions in 
**exercises/solutions/3_files_and_commands** are suitable for the input for 
letter "A" in **exercises/inputs/3_files_and_commands/A**. 

Problem 3.1
-----------

The input image files are some parts of 2 × 2 tiled capital letters. Some of 
the input tiles are rotated or mirrored vertically or horizontally and some missing 
tiles can be recovered by the same operations using the symmetry. The task is 
to recover all tiles and put them together to reconstruct the image of the 
selected letter.

Select a set of input images (**piece-1.png** and **piece-2.png**) for a letter 
from the folder **exercises/inputs/3_files_and_commands**. 
Then use the provided Fireworks and compose a workflow to reconstruct the 
selected letter by adjusting the image processing parameters, inputs and outputs. 
Verify, add and run the workflow and check the resulting image.


Problem 3.2
-----------

Given an image as input, "swirl" the image at the angles 90°, 180°, 270° and 360°
producing four new images. Arrange the original and the resulting images in the 
sequence: 

0° → 90° → 180° → 270° → 360° → 270° → 180° → 90° → 0°

and make an animation. As input you can use either the image file **letter.png**
from **exercises/inputs/3_files_and_commands** or the reconstructed image from
**Problem 3.1**.
