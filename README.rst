Scientific workflow is an important technique used in many simulation and data 
analysis applications. In particular, workflows automate high-throughput / 
high-complexity computing applications, enable code and data reuse and 
provenance, provide methods for validation and error tracking, and exploit 
application concurrency using distributed computing resources. The goal of this 
tutorial is to learn composing and running workflow applications using the 
FireWorks workflow environment (https://hackingmaterials.lbl.gov/fireworks). In the 
first part, after an introduction to the concept of workflows, to 
state-of-the-art workflow systems and to FireWorks, the participants will learn 
to construct workflows using a library of existing Firetasks. The composed 
workflows will be verified, visualized and then executed and monitored. The 
exercises include managing data and control flow dynamically using 
FWAction. The last part focuses on writing custom Firetasks to match more 
specific application requirements. 

Basic knowledge of using the bash shell is required. For the last part, basic 
knowledge of Python is required.